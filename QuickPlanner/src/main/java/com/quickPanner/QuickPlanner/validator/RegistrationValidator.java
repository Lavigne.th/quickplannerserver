package com.quickPanner.QuickPlanner.validator;

import com.quickPanner.QuickPlanner.dto.auth.UserRegistrationRequest;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RegistrationValidator implements Validator {

    public static final int MIN_PSEUDO_LENGTH = 5;
    public static final int MAX_PSEUDO_LENGTH = 20;

    public static final int MIN_PASSWORD_LENGTH = 5;
    public static final int MAX_PASSWORD_LENGTH = 20;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserRegistrationRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserRegistrationRequest userDetails =(UserRegistrationRequest) target;

        validatePseudo(userDetails.getPseudo(), errors);
        validatePassword(userDetails.getPassword(), errors);
        validateEmail(userDetails.getEmail(), errors);
    }

    private void validateEmail(String email, Errors errors) {
        if(email != null && email.length() > 0 && !EmailValidator.getInstance().isValid(email)) {
            errors.rejectValue("email", "email.invalid");
        }
    }

    private void validatePassword(String password, Errors errors) {
        if(password == null) {
            errors.rejectValue("password", "password.null");
        } else {
            ValidationUtils.rejectIfEmpty(errors, "password", "pseudo.empty");
            if(password.length() < MIN_PASSWORD_LENGTH) {
                errors.rejectValue("password", "password.length.tooshort");
            } else if(password.length() > MAX_PASSWORD_LENGTH) {
                errors.rejectValue("password", "password.length.toolong");
            }
        }
    }

    private void validatePseudo(String pseudo, Errors errors) {
        if(pseudo == null) {
            errors.rejectValue("pseudo", "pseudo.null");
        } else {
            ValidationUtils.rejectIfEmpty(errors, "pseudo", "pseudo.empty");
            if(pseudo.length() < MIN_PSEUDO_LENGTH) {
                errors.rejectValue("pseudo", "pseudo.length.tooshort");
            } else if(pseudo.length() > MAX_PSEUDO_LENGTH) {
                errors.rejectValue("pseudo", "pseudo.length.toolong");
            }
        }
    }
}
