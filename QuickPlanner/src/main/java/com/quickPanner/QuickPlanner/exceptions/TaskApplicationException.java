package com.quickPanner.QuickPlanner.exceptions;

public class TaskApplicationException extends Exception {

    public TaskApplicationException(String message) {
        super(message);
    }
}
