package com.quickPanner.QuickPlanner.dto.auth;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.quickPanner.QuickPlanner.dto.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

public class LoginResponse extends BaseResponse {

    private String token;

    public LoginResponse(String token) {
        super(false, null);
        this.token = token;
    }

    public LoginResponse(boolean isError, String errorMessage) {
        super(isError, errorMessage);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
