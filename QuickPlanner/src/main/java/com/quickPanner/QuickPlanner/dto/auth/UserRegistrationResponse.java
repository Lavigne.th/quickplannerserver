package com.quickPanner.QuickPlanner.dto.auth;

import com.quickPanner.QuickPlanner.dto.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class UserRegistrationResponse extends BaseResponse {

    private String pseudo;

    private String email;

    public UserRegistrationResponse(boolean isError, String errorMessage) {
        super(isError, errorMessage);
    }

    public UserRegistrationResponse(String pseudo, String email) {
        super(false, null);
        this.pseudo = pseudo;
        this.email = email;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
