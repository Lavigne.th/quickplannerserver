package com.quickPanner.QuickPlanner.dto.account;

import com.quickPanner.QuickPlanner.dto.BaseResponse;

public class DeleteAccountResponse extends BaseResponse {

    public DeleteAccountResponse() {
        super(false, null);
    }

    public DeleteAccountResponse(boolean isError, String errorMessage) {
        super(isError, errorMessage);
    }
}
