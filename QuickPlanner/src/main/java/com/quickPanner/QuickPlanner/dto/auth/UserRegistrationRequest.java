package com.quickPanner.QuickPlanner.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationRequest {

    @NotNull
    @Length(min = 5, max = 20)
    private String pseudo;

    @NotNull
    @Length(min = 5, max = 20)
    private String password;

    @Email
    private String email;
}
