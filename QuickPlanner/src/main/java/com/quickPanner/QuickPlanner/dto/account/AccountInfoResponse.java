package com.quickPanner.QuickPlanner.dto.account;


import com.quickPanner.QuickPlanner.dto.BaseResponse;

public class AccountInfoResponse extends BaseResponse {

    private String pseudo;

    private String email;

    private int nbTasks;

    public AccountInfoResponse(String pseudo, String email, int nbTasks) {
        super(false, null);
        this.pseudo = pseudo;
        this.email = email;
        this.nbTasks = nbTasks;
    }

    public AccountInfoResponse(boolean isError, String errorMessage) {
        super(isError, errorMessage);
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNbTasks() {
        return nbTasks;
    }

    public void setNbTasks(int nbTasks) {
        this.nbTasks = nbTasks;
    }
}
