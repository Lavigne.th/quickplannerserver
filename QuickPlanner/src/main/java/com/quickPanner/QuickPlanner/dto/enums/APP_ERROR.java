package com.quickPanner.QuickPlanner.dto.enums;

public enum APP_ERROR {
    USER_ALREADY_EXISTS("user.alreadyExists"),
    WRONG_CREDENTIALS("credentials.wrong"),
    ACCOUNT_NOT_FOUND("account.notFound");

    private final String errorType;

    APP_ERROR(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorType() {
        return errorType;
    }
}
