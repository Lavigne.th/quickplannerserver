package com.quickPanner.QuickPlanner.dto.task;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetAllTasksRequest {

    @NotNull
    private Instant startDate;

}
