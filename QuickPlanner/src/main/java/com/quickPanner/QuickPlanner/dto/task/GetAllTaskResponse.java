package com.quickPanner.QuickPlanner.dto.task;

import com.quickPanner.QuickPlanner.dto.BaseResponse;

import java.util.List;

public class GetAllTaskResponse extends BaseResponse {

    private List<TaskDao> tasks;

    public GetAllTaskResponse(boolean isError, String errorMessage) {
        super(isError, errorMessage);
    }

    public GetAllTaskResponse(List<TaskDao> tasks) {
        super(false, null);
        this.tasks = tasks;
    }

    public List<TaskDao> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskDao> tasks) {
        this.tasks = tasks;
    }
}
