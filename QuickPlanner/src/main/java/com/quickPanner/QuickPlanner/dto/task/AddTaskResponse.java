package com.quickPanner.QuickPlanner.dto.task;

import com.quickPanner.QuickPlanner.dto.BaseResponse;

public class AddTaskResponse extends BaseResponse {

    private TaskDao task;

    public AddTaskResponse(boolean isError, String errorMessage) {
        super(isError, errorMessage);
    }

    public AddTaskResponse(TaskDao task) {
        super(false, null);
        this.task = task;
    }

    public TaskDao getTask() {
        return task;
    }

    public void setTask(TaskDao task) {
        this.task = task;
    }
}
