package com.quickPanner.QuickPlanner.service;

import com.quickPanner.QuickPlanner.dto.task.AddTaskResponse;
import com.quickPanner.QuickPlanner.dto.task.GetAllTaskResponse;
import com.quickPanner.QuickPlanner.dto.task.GetAllTasksRequest;
import com.quickPanner.QuickPlanner.dto.task.TaskDao;
import com.quickPanner.QuickPlanner.models.Task;
import com.quickPanner.QuickPlanner.models.User;
import com.quickPanner.QuickPlanner.repository.TaskRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskService {

    @Autowired
    private AuthService authService;

    @Autowired
    private TaskRepository taskRepository;

    public GetAllTaskResponse getAllTask(GetAllTasksRequest request) {
        User currentUser = authService.getCurrentUser();
        Instant endDate = Instant.from(request.getStartDate()).plus(7, ChronoUnit.DAYS);
        List<Task> tasks = taskRepository.findAllTaskByUserAndStartDate(request.getStartDate(), endDate, currentUser.getId());
        List<TaskDao> taskDaos = new ArrayList<>();
        for(Task task : tasks)
            taskDaos.add(new TaskDao(task.getId(),
                    task.getTitle(),
                    task.getDescription(),
                    task.getStartTime(),
                    task.getEndTime(),
                    task.getPrimaryColor(),
                    task.getSecondaryColor()
            ));

        return new GetAllTaskResponse(taskDaos);
    }

    public AddTaskResponse addTask(TaskDao taskDao) {
        User currentUser = authService.getCurrentUser();
        Task newTask = new Task(null,
                taskDao.getTitle(),
                taskDao.getDescription(),
                taskDao.getStartTime(),
                taskDao.getEndTime(),
                taskDao.getPrimaryColor(),
                taskDao.getSecondaryColor(),
                currentUser
        );
        newTask = taskRepository.save(newTask);
        taskDao.setId(newTask.getId());
        return new AddTaskResponse(taskDao);
    }

}
