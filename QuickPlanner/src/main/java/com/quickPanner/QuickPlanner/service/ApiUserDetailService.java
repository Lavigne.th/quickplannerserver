package com.quickPanner.QuickPlanner.service;

import com.quickPanner.QuickPlanner.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ApiUserDetailService implements UserDetailsService {

    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.quickPanner.QuickPlanner.models.User user = userRepository.findByPseudo(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return new User(user.getPseudo(), user.getPassword(), new ArrayList<>());
    }
}
