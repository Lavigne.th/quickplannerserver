package com.quickPanner.QuickPlanner.service;

import com.quickPanner.QuickPlanner.dto.account.AccountInfoResponse;
import com.quickPanner.QuickPlanner.dto.account.DeleteAccountResponse;
import com.quickPanner.QuickPlanner.dto.enums.APP_ERROR;
import com.quickPanner.QuickPlanner.models.User;
import com.quickPanner.QuickPlanner.repository.TaskRepository;
import com.quickPanner.QuickPlanner.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaskRepository taskRepository;

    public AccountInfoResponse getInfo() {
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> user = userRepository.findByPseudo(userDetails.getUsername());
        if(user.isPresent()) {
            int nbTasks = taskRepository.countByUserId(user.get().getId());
            return new AccountInfoResponse(user.get().getPseudo(), user.get().getEmail(), nbTasks);
        }
        return new AccountInfoResponse(true, APP_ERROR.ACCOUNT_NOT_FOUND.getErrorType());
    }

    @Transactional
    public DeleteAccountResponse deleteAccount() {
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> user = userRepository.findByPseudo(userDetails.getUsername());
        if(user.isPresent()) {
            taskRepository.deleteAllByUser(user.get().getId());
            userRepository.delete(user.get());

            return new DeleteAccountResponse();
        }
        return new DeleteAccountResponse(true, APP_ERROR.ACCOUNT_NOT_FOUND.getErrorType());
    }
}
