package com.quickPanner.QuickPlanner.service;

import com.quickPanner.QuickPlanner.dto.auth.LoginRequest;
import com.quickPanner.QuickPlanner.dto.auth.LoginResponse;
import com.quickPanner.QuickPlanner.dto.auth.UserRegistrationRequest;
import com.quickPanner.QuickPlanner.dto.auth.UserRegistrationResponse;
import com.quickPanner.QuickPlanner.dto.enums.APP_ERROR;
import com.quickPanner.QuickPlanner.models.User;
import com.quickPanner.QuickPlanner.repository.UserRepository;
import com.quickPanner.QuickPlanner.security.JwtUtility;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    private final JwtUtility jwtUtility;

    private final ApiUserDetailService apiUserDetailService;

    public UserRegistrationResponse signup(UserRegistrationRequest userDetails) {
        User user = new User();
        user.setPseudo(userDetails.getPseudo());
        user.setEmail(userDetails.getEmail() == null || userDetails.getEmail().length() == 0 ? null : userDetails.getEmail());
        user.setPassword(passwordEncoder.encode(userDetails.getPassword()));

        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            return new UserRegistrationResponse(true, APP_ERROR.USER_ALREADY_EXISTS.getErrorType());
        }
        return new UserRegistrationResponse(user.getPseudo(), user.getEmail());
    }

    public LoginResponse login(LoginRequest loginRequest) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getPseudo(),
                            loginRequest.getPassword()
                    )
            );
        } catch (Exception e) {
            return new LoginResponse(true, APP_ERROR.WRONG_CREDENTIALS.getErrorType());
        }


        final String token = jwtUtility.generateToken(apiUserDetailService.loadUserByUsername(loginRequest.getPseudo()));
        return new LoginResponse(token);
    }

    public User getCurrentUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByPseudo(userDetails.getUsername()).orElseThrow(() -> new UsernameNotFoundException("Current user not found"));
    }

}
