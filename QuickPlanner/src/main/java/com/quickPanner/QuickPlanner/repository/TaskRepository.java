package com.quickPanner.QuickPlanner.repository;

import com.quickPanner.QuickPlanner.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query(value = "SELECT * FROM task WHERE task.start_time >= :startDate AND task.start_time <= :endDate AND task.user_id = :userId", nativeQuery = true)
    List<Task> findAllTaskByUserAndStartDate(@Param("startDate") Instant startDate, @Param("endDate")  Instant endDate, @Param("userId") Long userId);

    @Modifying
    @Query(value = "DELETE FROM task WHERE task.user_id = :userId", nativeQuery = true)
    void deleteAllByUser(@Param("userId") Long userId);

    @Query(value = "SELECT COUNT(task.id) FROM quick_planner.task where task.user_id = :userId", nativeQuery = true)
    int countByUserId(@Param("userId") Long userId);
}
