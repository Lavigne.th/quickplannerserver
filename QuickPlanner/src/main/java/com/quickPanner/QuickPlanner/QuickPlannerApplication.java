package com.quickPanner.QuickPlanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuickPlannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuickPlannerApplication.class, args);
	}

}
