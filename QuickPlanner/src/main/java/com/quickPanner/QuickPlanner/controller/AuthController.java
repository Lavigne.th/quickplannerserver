package com.quickPanner.QuickPlanner.controller;

import com.quickPanner.QuickPlanner.dto.auth.LoginRequest;
import com.quickPanner.QuickPlanner.dto.auth.LoginResponse;
import com.quickPanner.QuickPlanner.dto.auth.UserRegistrationRequest;
import com.quickPanner.QuickPlanner.dto.auth.UserRegistrationResponse;
import com.quickPanner.QuickPlanner.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/signup")
    public ResponseEntity<UserRegistrationResponse> signup(@Valid @RequestBody UserRegistrationRequest userDetails){
        return ResponseEntity.status(HttpStatus.OK).body(
                authService.signup(userDetails)
        );
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(
                authService.login(loginRequest)
        );
    }
}
