package com.quickPanner.QuickPlanner.controller;

import com.quickPanner.QuickPlanner.dto.task.AddTaskResponse;
import com.quickPanner.QuickPlanner.dto.task.GetAllTaskResponse;
import com.quickPanner.QuickPlanner.dto.task.GetAllTasksRequest;
import com.quickPanner.QuickPlanner.dto.task.TaskDao;
import com.quickPanner.QuickPlanner.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/task")
@AllArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @PostMapping("/tasks")
    public ResponseEntity<GetAllTaskResponse> getAllTask(@Valid @RequestBody GetAllTasksRequest request) {
        return ResponseEntity.status(HttpStatus.OK).body(taskService.getAllTask(request));
    }

    @PostMapping("/add")
    public ResponseEntity<AddTaskResponse> getAllTask(@Valid @RequestBody TaskDao request) {
        return ResponseEntity.status(HttpStatus.OK).body(taskService.addTask(request));
    }
}
