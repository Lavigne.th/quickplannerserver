package com.quickPanner.QuickPlanner.controller;

import com.quickPanner.QuickPlanner.dto.account.AccountInfoResponse;
import com.quickPanner.QuickPlanner.dto.account.DeleteAccountResponse;
import com.quickPanner.QuickPlanner.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.pl.NIP;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/account")
@AllArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/info")
    public ResponseEntity<AccountInfoResponse> accountInfo() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.getInfo());
    }

    @GetMapping("/delete")
    public ResponseEntity<DeleteAccountResponse> deleteAccount() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(accountService.deleteAccount());
    }
}
